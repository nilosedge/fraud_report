class Admin::AdminController < ApplicationController

	layout "admin"

	def index
	end
	def show
	end

	def charts
		render "charts"
	end
end
